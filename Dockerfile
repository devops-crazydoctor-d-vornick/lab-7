FROM golang:1.16-alpine as system

FROM system as build

WORKDIR /app

COPY go.mod ./
COPY main.go ./

RUN go mod download
# Сборка независимо от окружения
RUN CGO_ENABLED=0 GOOS=linux go build -o main

FROM scratch
WORKDIR /

COPY --from=build /app/main /main

ENTRYPOINT ["/main"]